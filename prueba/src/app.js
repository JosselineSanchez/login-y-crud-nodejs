"use strict"

const express = require('express')
const path = require('path')
const morgan = require('morgan')
const bodyParser = require("body-parser")
const mongoose = require("mongoose");
//const passport = require('passport')
let port = 2000

// Ejecutar express
let app = express()

const routes = require('./routes/index')

//Config
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'ejs')

//Middlewares
app.use(morgan('dev'))
app.use(bodyParser.urlencoded({extended:false}))
app.use(bodyParser.json())
app.use(express.static('public'))
/*app.use(passport.initialize())
app.use(passport.session())*/
//CORS
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
    next();
});

//Rutas
app.use('/static', express.static(path.join(__dirname, 'public')));
app.use('/', routes)

//Forzar metodos antiguos para trabajar con mongo
mongoose.set('useFindAndModify', false)

//Trabajar con promesa
mongoose.Promise = global.Promise;

//Para la conexion
let url = "mongodb://localhost:27017/crudAuto"; //localhost:puerto/nombreBD
let opciones = {
    useNewUrlParser: true,
    useUnifiedTopology: true
};

//Crear conexion
mongoose.connect(url, opciones)
.then(()=>{
    console.log("Conexión exitosa!!!");
    app.listen(port, () => {
        console.log(`Servidor corriendo en http://localhost:${port}`);
    })
})
.catch((err) => console.log(err))
