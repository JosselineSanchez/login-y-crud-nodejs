'use strict'
const express = require('express')
const passport = require('passport')
const router = express.Router()
let controller = require('../controllers/auto')
let controllerUser = require('../controllers/user')

//Rutas de prueba
router.get('/', controllerUser.inicio)
router.post('/login', controllerUser.login)
router.get('/menu', controllerUser.menu)
router.get('/auto', controller.auto)
router.post('/insert', controller.insert)
router.get('/delete/:id', controller.delete)
router.get('/update/:id', controller.update)
router.post('/edit/:id', controller.edit)
router.get('/salir', controller.salir)
module.exports = router