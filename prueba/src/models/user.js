// modelo clase defirnir propiedades
'use strict'
let mongoose = require("mongoose")
let Schema = mongoose.Schema
let UserSchema = Schema({
    user:{
        type: String,
        required: true,
        unique: true
    },
    password:{
        type: String,
        required: true
    }
})
// Exportar nombreDocumento, modeloDocumento
module.exports=mongoose.model('user', UserSchema)
//articles --> guarda documentos de este tipo y con estructura dentro de la colección