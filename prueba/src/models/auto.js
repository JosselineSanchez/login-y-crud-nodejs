// modelo clase defirnir propiedades
'use strict'
let mongoose = require("mongoose")
let Schema = mongoose.Schema
let AutoSchema = Schema({
    codigo:{
        type: String,
        required: true
    },
    content:String,
    date:{
        type:Date,
        default: Date.now
    },
    precio:{
        type: Number,
        required: true
    }
})
// Exportar nombreDocumento, modeloDocumento
module.exports=mongoose.model('auto', AutoSchema)
//articles --> guarda documentos de este tipo y con estructura dentro de la colección