"use strict"

const express = require('express')
let fs = require('fs')
let path = require('path')
let validator = require('validator')
let Auto = require('../models/auto')

let controller ={
    auto: async (req, res)=>{
        var autos = await Auto.find()
        console.log(autos);
        res.render('crud',{
            autos,
            message: ""
        })
    },
    insert: async(req, res) => {
        let params = req.body
        console.log(params);
        try{
            var validate_codigo = !validator.isEmpty(params.codigo)
            var validate_content = !validator.isEmpty(params.content)
            var validate_precio = !validator.isEmpty(params.precio)
        }catch(error){
            console.log(error);
            res.render('crud',{
                autos: await Auto.find(),
                message: "Faltan datos por enviar"
            })
        }
        if(validate_codigo && validate_content && validate_precio){
            //crear el objeto a guardar
            let auto = new Auto()
            //asignar valore
            auto.codigo = params.codigo
            auto.content = params.content
            auto.precio = params.precio
            //guardar articulos
            auto.save((err, autoStored)=>{
                if(err || !autoStored){
                    return res.status(404).send({
                        status: "error",
                        message: "El auto no se ah guardado !!"
                    })
                }else{
                    //devolver una respuesta
                    return res.status(200).redirect('/auto')
                }
            })
        }else{
            res.render('crud',{
                autos: await Auto.find(),
                message: "Los datos no son válidos o faltan campos por llenar"
            })
        }
    },
    delete: async(req, res) => {
        const { id } = req.params
        await Auto.remove({_id: id})
        return res.status(200).redirect('/auto')
    },
    update: async(req, res) => {
        const { id } = req.params
        let auto = await Auto.findById({_id: id})
        res.render('edit', {
            auto
        })
    },
    edit: async (req, res) => {
        const { id } = req.params
        await Auto.update({_id : id}, req.body)
        return res.status(200).redirect('/auto')
    },
    salir: (req, res)=>{
        res.redirect('salir')
    }
}

module.exports = controller