"use strict"

const express = require('express')
let fs = require('fs')
let path = require('path')
const { db } = require('../models/user')
const User = require('../models/user')

let controller ={
    inicio: (req, res) => {
        res.render('index',{
            message: ""
        })
    },
    login: async(req, res) => {
        let params = req.body
        let user = await db.collection('user').findOne({
            user: params.user
        })
        if (!user){
            res.render('index',{
                message: "Usuario incorrecto !!"
            })
        }else{
            if(user.password == params.password)
                res.render('menu')
            else{
                res.render('index',{
                    message: "Clave incorrecta !!"
                })
            }
        }
    },
    menu: (req, res)=>{
        res.render('menu')
    },
}

module.exports = controller